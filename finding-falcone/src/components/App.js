import React, { Component } from 'react';
import '../assets/css/App.css';
import logo from '../assets/img/logo.png'
import AppRouter from '../routes'

export default class App extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <h1>
                        Finding Falcone
                    </h1>
                    <img src={logo}/>
                </header>

                <div className="App-body">
                    <AppRouter/>
                </div>

                <footer className="App-footer">
                    The app is made with React
                </footer>
            </div>
        );
    }
}
