import React from 'react';
import Selection from './components/views/selection';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Results from './components/views/result';

const AppRouter = () => (
    <Router>
        <div>
            <Route path="/" exact component={Selection} />
            <Route path="/results" component={Results} />
        </div>
    </Router>
);

export default AppRouter;