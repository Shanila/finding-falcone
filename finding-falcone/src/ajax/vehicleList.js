import React, { Component } from 'react';

class VehicleList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            vehicle: []
        };
    }

    componentDidMount() {
        fetch("https://findfalcone.herokuapp.com/vehicles")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        vehicle: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, vehicle } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="vehicleListContainer">
                    {vehicle.map(vehicle => (
                        <div>
                            <input type="radio" name="dest1Vehicle" value="{vehicle.speed}"/>{vehicle.name}({vehicle.total_no})
                        </div>
                    ))}
                </div>
            );
        }
    }
}

export default VehicleList;
