import React, { Component } from 'react';
import '../../assets/css/App.css';
import '../../assets/css/selection.css';
import PlanetList from '../..//ajax/planetList'
import VehicleList from '../../ajax/vehicleList'

class Selection extends Component {
    render() {
        return (
            <div>
                <h2>
                    Select the planets you want to search:
                </h2>

                <div className="planetListContainer">
                    <div className="destination1">
                        <h6>Destination 1</h6>
                        <PlanetList />
                        <VehicleList />
                    </div>

                    <div className="destination2">
                        <h6>Destination 2</h6>
                        <PlanetList />
                        <VehicleList />
                    </div>

                    <div className="destination3">
                        <h6>Destination 3</h6>
                        <PlanetList />
                        <VehicleList />
                    </div>

                    <div className="destination4">
                        <h6>Destination 4</h6>
                        <PlanetList />
                        <VehicleList />
                    </div>
                    <div>
                        <h6>Total Time:</h6>
                    </div>
                </div>
            </div>
        );
    }
}

export default Selection;
