import React, { Component } from 'react';

class PlanetList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            planet: []
        };
    }

    componentDidMount() {
        fetch("https://findfalcone.herokuapp.com/planets")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        planet: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, planet } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <select>
                    <option>Select</option>
                    {planet.map(planet => (
                        <option value={planet.name}>
                            {planet.name}
                        </option>
                    ))}
                </select>
            );
        }
    }
}

export default PlanetList;
