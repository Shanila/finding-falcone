import React, { Component } from 'react';
import '../../assets/css/result.css';

class Results extends Component {
    render() {
        return (
            <div className="result">
                <div>
                    You are <strong>successful</strong>
                </div>

                <div>
                Al Falcone is exiled for another 15 years.
                </div>

                <div>
                    You are <strong>unsuccessful</strong>
                </div>

                <div>
                    Al Falcone shall return at the end of term.
                </div>

            </div>
        );
    }
}

export default Results;
